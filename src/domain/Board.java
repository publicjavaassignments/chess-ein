package domain;

import javafx.scene.paint.Color;

/**
 *
 * @author tha
 */
public class Board {

    /**
     * The number of squares in a row (horizontally or vertically)
     */
    private final int NUMBEROFSQUARES = 8;
    /**
     * The size of a square in pixels
     */
    public static final int SIZEOFSQUARE = 75;

    private final Square[][] squares = new Square[NUMBEROFSQUARES][NUMBEROFSQUARES];


    public Board()
    {
        this.placeAllSquares();
        this.placeAllPieces();   //New
    }

    private void placeAllSquares(){
        int count = 0; // helper

        for (int row = 0; row < NUMBEROFSQUARES; row++)
        {
            for(int col = 0; col < NUMBEROFSQUARES; col++) //
            {
                count++;
                // System.out.println("count = "+count+", row = "+row+", col = "+col);
                if (count%2 ==0)
                {
                    squares[row][col] = new Square(SIZEOFSQUARE,Color.WHITE ,col*SIZEOFSQUARE, row*SIZEOFSQUARE);
                }else
                {
                    squares[row][col] = new Square(SIZEOFSQUARE, Color.INDIANRED,col*SIZEOFSQUARE, row*SIZEOFSQUARE);
                }
            }
            count++;
        }
    }

    // New
    private void placeAllPieces()
    {
        /// Setting all the black pieces
        squares[0][0].setPiece(new Rook("file:src/presentation/resources/Chess_rdt60.png", Color.BLACK));

        squares[0][1].setPiece(new Knight("file:src/presentation/resources/Chess_ndt60.png", Color.BLACK));

        squares[0][2].setPiece(new Piece("file:src/presentation/resources/Chess_bdt60.png", Color.BLACK));

        squares[0][3].setPiece(new Piece("file:src/presentation/resources/Chess_qdt60.png", Color.BLACK));

        squares[0][4].setPiece(new Piece("file:src/presentation/resources/Chess_kdt60.png", Color.BLACK));

        squares[0][5].setPiece(new Piece("file:src/presentation/resources/Chess_bdt60.png", Color.BLACK));

        squares[0][6].setPiece(new Knight("file:src/presentation/resources/Chess_ndt60.png", Color.BLACK));

        squares[0][7].setPiece(new Rook("file:src/presentation/resources/Chess_rdt60.png", Color.BLACK));

        squares[1][0].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][1].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][2].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][3].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][4].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][5].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][6].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));

        squares[1][7].setPiece(new Piece("file:src/presentation/resources/Chess_pdt60.png", Color.BLACK));


        // Setting all the white pieces
        squares[6][0].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][1].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][2].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][3].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][4].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][5].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][6].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[6][7].setPiece(new Piece("file:src/presentation/resources/Chess_plt60.png", Color.WHITE));

        squares[7][0].setPiece(new Rook("file:src/presentation/resources/Chess_rlt60.png", Color.WHITE));

        squares[7][1].setPiece(new Knight("file:src/presentation/resources/Chess_nlt60.png", Color.WHITE));

        squares[7][2].setPiece(new Piece("file:src/presentation/resources/Chess_blt60.png", Color.WHITE));

        squares[7][3].setPiece(new Piece("file:src/presentation/resources/Chess_qlt60.png", Color.WHITE));

        squares[7][4].setPiece(new Piece("file:src/presentation/resources/Chess_klt60.png", Color.WHITE));

        squares[7][5].setPiece(new Piece("file:src/presentation/resources/Chess_blt60.png", Color.WHITE));

        squares[7][6].setPiece(new Knight("file:src/presentation/resources/Chess_nlt60.png", Color.WHITE));

        squares[7][7].setPiece(new Rook("file:src/presentation/resources/Chess_rlt60.png", Color.WHITE));
    }

    /**
     * For drawing the board based on squares
     * @return an array of all square objects on this board
     */
    public Square[] getAllSquares() {
        int index = 0;
        Square [] result = new Square[NUMBEROFSQUARES*NUMBEROFSQUARES];

        for (int y = 0; y < NUMBEROFSQUARES; y++)
        {
            for(int x = 0; x < NUMBEROFSQUARES; x++)
            {
                result[index] = this.squares[y][x];
                index++;
            }
        }
        return result;
    }

    /**
     * Gets the square
     * @param x
     * @param y
     * @return
     */
    public Square getSquareAtPosition(int x, int y)
    {
        int posX = x/SIZEOFSQUARE;
        int posY = y/SIZEOFSQUARE;
        return this.squares[posY][posX];
    }

}
