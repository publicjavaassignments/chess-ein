package domain;
import domain.Piece;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 *
 * @author tha
 */
public class Rook extends Piece {

    public Rook(String imageUrl, Color color)
    {
        super(imageUrl, color);
    }

    @Override
    public ArrayList<Square> getPotentialMoves(Square s)
    {
        // Idk

        ArrayList<Square> res = new ArrayList<>();
        int indexX = s.getXIndex();
        int indexY = s.getYIndex();

        for (int i = 0; i < 8; i++)
        {
            if(i == indexX)
            {

            }else
            {
                res.add(new Square(Board.SIZEOFSQUARE, Color.RED, i * Board.SIZEOFSQUARE, indexY * Board.SIZEOFSQUARE));
            }
        }

        for (int i = 0; i < 8; i++)
        {
            if(i == indexX)
            {

            }else
            {
                res.add(new Square(Board.SIZEOFSQUARE, Color.RED, i * Board.SIZEOFSQUARE, indexY * Board.SIZEOFSQUARE));
            }
        }
        for (int y = 0; y < 8; y++)
    {
        if(y == indexX)
        {

        }else
        {
            res.add(new Square(Board.SIZEOFSQUARE, Color.RED, indexX * Board.SIZEOFSQUARE, y * Board.SIZEOFSQUARE));
        }
    }
        return res;
    }
   
    @Override
    public String toString(){
        return super.toString() + " Rook";
    }
}
